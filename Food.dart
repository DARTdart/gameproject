class Food {
  late int x;
  late int y;
  late String symbol;

  Food(int x, int y) {
    this.x = x;
    this.y = y;
    this.symbol = 'F';
  }

  int getX() {
    return x;
  }

  int getY() {
    return y;
  }

  String getSymbol() {
    return symbol;
  }

  bool isOn(int x, int y) {
    return (this.x == x) && (this.y == y);
  }
  
}