import 'Animal.dart';
import 'Map.dart';

class Dog extends Animal {

  late int x;
  late int y;
  late String symbol;
  late Map map;

  Dog(int x , int y , String symbol , Map map ) : super(x,y) {
    this.x = x;
    this.y = y;
    this.symbol = 'D';
    this.map = map;
  }

  int getX() {
    return x;
  }

  int getY() {
    return y;
  }

  String getSymbol() {
    return symbol;
  }

  void speak() {
    print('Bark! Bark !');
  }

  void bite() {
      print('Cat was bitten by Dog (˃̣̣̥⌓˂̣̣̥ )');
  }

}