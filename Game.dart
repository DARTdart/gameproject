import 'dart:io';
import 'Map.dart';
import 'Cat.dart';
import 'Dog.dart';
import 'Food.dart';
import 'Bomb.dart';

void main(List<String> arguments) {
  Map map = new Map(10, 5);
  Cat cat = new Cat(0, 0, 'C', map);
  Dog dog = new Dog(4, 3, 'D', map);
  Bomb bomb = new Bomb(5, 2);
  Food food = new Food(5, 3);

  map.setCat(cat);
  map.setDog(dog);
  map.setBomb(bomb);
  map.setFood(food);

  map.showWelcome();
  cat.speak();

  while (true) {
    map.showMap();
    if (map.getNum() == 1) {
      print('You Win ! , Congratulation ❤❤❤');
      break;
    } else if (map.getNum() == 2) {
      print('You lose ! , Give it a try  ♡♥♡♥♡♥');
      break;
    }
    print('Please input direction :');
    String direction = stdin.readLineSync()!;
    print('');
    if (direction == 'Q') {
      print('【 GOODBYE 】');
      print('【 Thank you for Playing 】');
      break;
    }
    cat.walk(direction);
  }

}