import 'dart:io';
import 'dart:math';
import 'Animal.dart';
import 'Map.dart';
import 'Dog.dart';

class Cat extends Animal {
  late int x;
  late int y;
  late String symbol;
  late Map map;
  late int num;

  Cat(int x, int y, String symbol, Map map) : super(x, y) {
    this.x = x;
    this.y = y;
    this.symbol = 'C';
    this.map = map;
  }

  int getX() {
    return x;
  }

  int getY() {
    return y;
  }

  String getSymbol() {
    return symbol;
  }

  int getNum() {
    return num;
  }
  
  void setNum(int num) {
    this.num = num;
  }

  void speak() {
    print('Meow ! Meow !');
  }

  void eat() {
    print('Cat has eaten (｡♥‿♥｡)');
  }

  bool isOn(int x, int y) {
    return (this.x == x) && (this.y == y);
  }


  bool walk(String direction) {
    switch (direction) {
      case 'N': //เหนือ
        if (walkN()) return false;
        break;

      case 'S': //ใต้
        if (walkS()) return false;
        break;

      case 'E': //ออก
        if (walkE()) return false;
        break;

      case 'W': //ตก
        if (walkW()) return false;
        break;

      default:
        return false;
    }
    Check();
    return true;
  }

  bool walkN() {
    if (map.inMap(x, y - 1)) {
      y = y - 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkS() {
    if (map.inMap(x, y + 1)) {
      y = y + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkE() {
    if (map.inMap(x + 1, y)) {
      x = x + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkW() {
    if (map.inMap(x - 1, y)) {
      x = x - 1;
    } else {
      return true;
    }
    return false;
  }

  void Check() {
    if (map.isBomb(x, y)) {
      print('Found Bomb : Cat is Dead (ಥ﹏ಥ)');
      map.setNum(2);
    } else if (map.isDog(x, y)) {
      map.dog.speak();
      map.dog.bite();
      map.setNum(2);
    } else if (map.isFood(x, y)) {
      eat();
      map.setNum(1);
    }   
  }

}