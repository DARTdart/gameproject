import 'dart:async';
import 'dart:io';

import 'Cat.dart';
import 'Dog.dart';
import 'Food.dart';
import 'Bomb.dart';
import 'Animal.dart';

class Map {
  late int width;
  late int height;
  late Cat cat;
  late Dog dog;
  late Food food;
  late Bomb bomb;
  late int num = 0;

  //constuctor
  Map(int width, int height) {
    this.width = width;
    this.height = height;
  }

  void setCat(Cat cat) {
    this.cat = cat;
  }

  void showCat() {
    stdout.write(cat.getSymbol());
  }

  void setDog(Dog dog) {
    this.dog = dog;
  }

  void showDog() {
    stdout.write(dog.getSymbol());
  }

  void setBomb(Bomb bomb) {
    this.bomb = bomb;
  }

  void showBomb() {
    stdout.write(cat.getSymbol());
  }

  void setFood(Food food) {
    this.food = food;
  }

  void showFood() {
    stdout.write(cat.getSymbol());
  }

  int getNum() {
    return num;
  }

  void setNum(int num) {
    this.num = num;
  }

  void showWelcome() {
    print('Welcome to Game');
    showRule();
    print('Are You Ready ?');
    print("Let's Start !");
    print('');
  }

  void showMap() {
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        if (cat.isOn(x, y)) {
          stdout.write(cat.getSymbol());
        } else if (food.isOn(x, y)) {
          stdout.write(food.getSymbol());
        } else {
          stdout.write("-");
        }
      }
      print('\n');
    }
  }

  int checkWin() {
    if (isFood(width, height)) {
      return 1;
    }
    return 0;
  }

  void showRule() {
    print('Rule :');
    print('     N - เดินขึ้น');
    print('     S - เดินลง');
    print('     W - เดินไปทางซ้าย');
    print('     E - เดินไปทางขวา');
    print('     Q - ออก');
  }

  bool isBomb(int x, int y) {
    return (bomb.getX() == x) && (bomb.getY() == y);
  }

  bool inMap(int x, int y) {
    return (x >= 0 && x < width) && (y >= 0 && y < height);
  }

  bool isFood(int x, int y) {
    return (food.getX() == x) && (food.getY() == y);
  }

  bool isDog(int x, int y) {
    return (dog.getX() == x) && (dog.getY() == y);
  }
}
